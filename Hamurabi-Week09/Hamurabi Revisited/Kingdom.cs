﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hamurabi_Revisited
{
    /// <summary>
    /// The Kingdom class serves to track the different objects - land, grain and 
    /// population - needed to run the simulation.
    /// </summary>
    class Kingdom
    {
        private Grain grain = new Grain();                  // Tracks the grain available (in bushels)
        private Land land = new Land();                     // Tracks the current amount of land (in acres)
        private Population population = new Population();   // Tracks the current population
        private int year = 0;                               // The current year

        /// <summary>
        /// Read-only instance of the Grain object.
        /// </summary>
        public Grain Grain
        {
            get
            {
                return this.grain;
            }
        }

        /// <summary>
        /// Read-only instance of the Land object
        /// </summary>
        public Land Land
        {
            get
            {
                return this.land;
            }
        }

        /// <summary>
        /// Read-only instance of the Population object
        /// </summary>
        public Population Population
        {
            get
            {
                return this.population;
            }
        }

        /// <summary>
        /// Read-only property to access the current year
        /// </summary>
        public int Year
        {
            get
            {
                return this.year;
            }
        }

        /// <summary>
        /// Increase the year by one.
        /// </summary>
        public void AdvanceYear()
        {
            this.year++;
        }
    }
}
