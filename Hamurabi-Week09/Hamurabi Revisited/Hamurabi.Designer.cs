﻿namespace Hamurabi_Revisited
{
    partial class Hamurabi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.YearLabel = new System.Windows.Forms.Label();
            this.PopulationLabel = new System.Windows.Forms.Label();
            this.GrainLabel = new System.Windows.Forms.Label();
            this.AcresLabel = new System.Windows.Forms.Label();
            this.SellLabel = new System.Windows.Forms.Label();
            this.BuyLabel = new System.Windows.Forms.Label();
            this.SowLabel = new System.Windows.Forms.Label();
            this.FoodLabel = new System.Windows.Forms.Label();
            this.YearButton = new System.Windows.Forms.Button();
            this.Population = new System.Windows.Forms.TextBox();
            this.Grain = new System.Windows.Forms.TextBox();
            this.Acres = new System.Windows.Forms.TextBox();
            this.AcresToSow = new System.Windows.Forms.TextBox();
            this.AcresToBuy = new System.Windows.Forms.TextBox();
            this.AcresToSell = new System.Windows.Forms.TextBox();
            this.Food = new System.Windows.Forms.TextBox();
            this.Deaths = new System.Windows.Forms.TextBox();
            this.Births = new System.Windows.Forms.TextBox();
            this.DeathsLabel = new System.Windows.Forms.Label();
            this.BirthsLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // YearLabel
            // 
            this.YearLabel.AutoSize = true;
            this.YearLabel.Location = new System.Drawing.Point(221, 9);
            this.YearLabel.Name = "YearLabel";
            this.YearLabel.Size = new System.Drawing.Size(41, 13);
            this.YearLabel.TabIndex = 0;
            this.YearLabel.Text = "Year: 0";
            // 
            // PopulationLabel
            // 
            this.PopulationLabel.AutoSize = true;
            this.PopulationLabel.Location = new System.Drawing.Point(82, 42);
            this.PopulationLabel.Name = "PopulationLabel";
            this.PopulationLabel.Size = new System.Drawing.Size(57, 13);
            this.PopulationLabel.TabIndex = 1;
            this.PopulationLabel.Text = "Population";
            // 
            // GrainLabel
            // 
            this.GrainLabel.AutoSize = true;
            this.GrainLabel.Location = new System.Drawing.Point(107, 65);
            this.GrainLabel.Name = "GrainLabel";
            this.GrainLabel.Size = new System.Drawing.Size(32, 13);
            this.GrainLabel.TabIndex = 2;
            this.GrainLabel.Text = "Grain";
            // 
            // AcresLabel
            // 
            this.AcresLabel.AutoSize = true;
            this.AcresLabel.Location = new System.Drawing.Point(105, 88);
            this.AcresLabel.Name = "AcresLabel";
            this.AcresLabel.Size = new System.Drawing.Size(34, 13);
            this.AcresLabel.TabIndex = 3;
            this.AcresLabel.Text = "Acres";
            // 
            // SellLabel
            // 
            this.SellLabel.AutoSize = true;
            this.SellLabel.Location = new System.Drawing.Point(73, 180);
            this.SellLabel.Name = "SellLabel";
            this.SellLabel.Size = new System.Drawing.Size(66, 13);
            this.SellLabel.TabIndex = 4;
            this.SellLabel.Text = "Acres to Sell";
            // 
            // BuyLabel
            // 
            this.BuyLabel.AutoSize = true;
            this.BuyLabel.Location = new System.Drawing.Point(72, 203);
            this.BuyLabel.Name = "BuyLabel";
            this.BuyLabel.Size = new System.Drawing.Size(67, 13);
            this.BuyLabel.TabIndex = 5;
            this.BuyLabel.Text = "Acres to Buy";
            // 
            // SowLabel
            // 
            this.SowLabel.AutoSize = true;
            this.SowLabel.Location = new System.Drawing.Point(69, 227);
            this.SowLabel.Name = "SowLabel";
            this.SowLabel.Size = new System.Drawing.Size(70, 13);
            this.SowLabel.TabIndex = 6;
            this.SowLabel.Text = "Acres to Sow";
            // 
            // FoodLabel
            // 
            this.FoodLabel.AutoSize = true;
            this.FoodLabel.Location = new System.Drawing.Point(108, 249);
            this.FoodLabel.Name = "FoodLabel";
            this.FoodLabel.Size = new System.Drawing.Size(31, 13);
            this.FoodLabel.TabIndex = 7;
            this.FoodLabel.Text = "Food";
            // 
            // YearButton
            // 
            this.YearButton.Location = new System.Drawing.Point(108, 287);
            this.YearButton.Name = "YearButton";
            this.YearButton.Size = new System.Drawing.Size(83, 21);
            this.YearButton.TabIndex = 8;
            this.YearButton.Text = "Advance Year";
            this.YearButton.UseVisualStyleBackColor = true;
            this.YearButton.Click += new System.EventHandler(this.yearButton_Click);
            // 
            // Population
            // 
            this.Population.Enabled = false;
            this.Population.Location = new System.Drawing.Point(146, 38);
            this.Population.Name = "Population";
            this.Population.Size = new System.Drawing.Size(65, 20);
            this.Population.TabIndex = 9;
            // 
            // Grain
            // 
            this.Grain.Enabled = false;
            this.Grain.Location = new System.Drawing.Point(146, 62);
            this.Grain.Name = "Grain";
            this.Grain.Size = new System.Drawing.Size(65, 20);
            this.Grain.TabIndex = 10;
            // 
            // Acres
            // 
            this.Acres.Enabled = false;
            this.Acres.Location = new System.Drawing.Point(146, 85);
            this.Acres.Name = "Acres";
            this.Acres.Size = new System.Drawing.Size(65, 20);
            this.Acres.TabIndex = 11;
            // 
            // AcresToSow
            // 
            this.AcresToSow.Location = new System.Drawing.Point(145, 224);
            this.AcresToSow.Name = "AcresToSow";
            this.AcresToSow.Size = new System.Drawing.Size(65, 20);
            this.AcresToSow.TabIndex = 14;
            // 
            // AcresToBuy
            // 
            this.AcresToBuy.Location = new System.Drawing.Point(145, 201);
            this.AcresToBuy.Name = "AcresToBuy";
            this.AcresToBuy.Size = new System.Drawing.Size(65, 20);
            this.AcresToBuy.TabIndex = 13;
            // 
            // AcresToSell
            // 
            this.AcresToSell.Location = new System.Drawing.Point(145, 177);
            this.AcresToSell.Name = "AcresToSell";
            this.AcresToSell.Size = new System.Drawing.Size(65, 20);
            this.AcresToSell.TabIndex = 12;
            // 
            // Food
            // 
            this.Food.Location = new System.Drawing.Point(145, 246);
            this.Food.Name = "Food";
            this.Food.Size = new System.Drawing.Size(65, 20);
            this.Food.TabIndex = 15;
            // 
            // Deaths
            // 
            this.Deaths.Enabled = false;
            this.Deaths.Location = new System.Drawing.Point(145, 141);
            this.Deaths.Name = "Deaths";
            this.Deaths.Size = new System.Drawing.Size(65, 20);
            this.Deaths.TabIndex = 19;
            // 
            // Births
            // 
            this.Births.Enabled = false;
            this.Births.Location = new System.Drawing.Point(145, 118);
            this.Births.Name = "Births";
            this.Births.Size = new System.Drawing.Size(65, 20);
            this.Births.TabIndex = 18;
            // 
            // DeathsLabel
            // 
            this.DeathsLabel.AutoSize = true;
            this.DeathsLabel.Location = new System.Drawing.Point(98, 144);
            this.DeathsLabel.Name = "DeathsLabel";
            this.DeathsLabel.Size = new System.Drawing.Size(41, 13);
            this.DeathsLabel.TabIndex = 17;
            this.DeathsLabel.Text = "Deaths";
            // 
            // BirthsLabel
            // 
            this.BirthsLabel.AutoSize = true;
            this.BirthsLabel.Location = new System.Drawing.Point(106, 121);
            this.BirthsLabel.Name = "BirthsLabel";
            this.BirthsLabel.Size = new System.Drawing.Size(33, 13);
            this.BirthsLabel.TabIndex = 16;
            this.BirthsLabel.Text = "Births";
            // 
            // Hamurabi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 324);
            this.Controls.Add(this.Deaths);
            this.Controls.Add(this.Births);
            this.Controls.Add(this.DeathsLabel);
            this.Controls.Add(this.BirthsLabel);
            this.Controls.Add(this.Food);
            this.Controls.Add(this.AcresToSow);
            this.Controls.Add(this.AcresToBuy);
            this.Controls.Add(this.AcresToSell);
            this.Controls.Add(this.Acres);
            this.Controls.Add(this.Grain);
            this.Controls.Add(this.Population);
            this.Controls.Add(this.YearButton);
            this.Controls.Add(this.FoodLabel);
            this.Controls.Add(this.SowLabel);
            this.Controls.Add(this.BuyLabel);
            this.Controls.Add(this.SellLabel);
            this.Controls.Add(this.AcresLabel);
            this.Controls.Add(this.GrainLabel);
            this.Controls.Add(this.PopulationLabel);
            this.Controls.Add(this.YearLabel);
            this.Name = "Hamurabi";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label YearLabel;
        private System.Windows.Forms.Label PopulationLabel;
        private System.Windows.Forms.Label GrainLabel;
        private System.Windows.Forms.Label AcresLabel;
        private System.Windows.Forms.Label SellLabel;
        private System.Windows.Forms.Label BuyLabel;
        private System.Windows.Forms.Label SowLabel;
        private System.Windows.Forms.Label FoodLabel;
        private System.Windows.Forms.Button YearButton;
        private System.Windows.Forms.TextBox Population;
        private System.Windows.Forms.TextBox Grain;
        private System.Windows.Forms.TextBox Acres;
        private System.Windows.Forms.TextBox AcresToSow;
        private System.Windows.Forms.TextBox AcresToBuy;
        private System.Windows.Forms.TextBox AcresToSell;
        private System.Windows.Forms.TextBox Food;
        private System.Windows.Forms.TextBox Deaths;
        private System.Windows.Forms.TextBox Births;
        private System.Windows.Forms.Label DeathsLabel;
        private System.Windows.Forms.Label BirthsLabel;
    }
}

