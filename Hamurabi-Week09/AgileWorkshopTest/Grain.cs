﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgileWorkshopTest
{
    /// <summary>
    /// The Grain object tracks the amount of grain (in bushels) that is currently available.
    /// </summary>
    class Grain
    {
        private int grain = 2800;       // Tracks the amount of grain available. 2800 is the default value.

        /// <summary>
        /// Read/write property representing the number of bushes available.
        /// </summary>
        public int GrainAvailable
        {
            set
            {
                this.grain = value;
            }
            get
            {
                return this.grain;
            }
        }
    }
}
