﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgileWorkshopTest
{
    /// <summary>
    /// Population manages the total number of people in the kingdom.
    /// In particular, it tracks births, deaths, and the number of people
    /// who survive each year.
    /// </summary>
    class Population
    {
        private int peasants = 95;  // Current population. Default is 95 people.
        private int deaths = 0;     // How many people died last year.
        private int births = 0;     // How many people were born last year.

        /// <summary>
        /// Read/write value to track the current population
        /// </summary>
        public int Peasants
        {
            set
            {
                this.peasants = value;
            }
            get
            {
                return this.peasants;
            }
        }

        /// <summary>
        /// Read-only value representing the number of births in the previous year
        /// </summary>
        public int Births
        {
            get
            {
                return this.births;
            }

        }

        /// <summary>
        /// Read-only value representing the number of deaths in the previous year
        /// </summary>
        public int Deaths
        {
            get
            {
                return this.deaths;
            }
        }

        /// <summary>
        /// Calculates and records the number of people who lived, were born and died 
        /// for a given year, based on how much food was provided for them.
        /// </summary>
        /// <param name="food">The amount of grain to use to feed the population</param>
        /// <param name="grain">The Grain object from which the food is to be subtracted</param>
        public void FeedPopulation(int food, Grain grain)
        {
            this.births = 0;
            this.deaths = 0;

            // Each person needs 20 bushes of grain to survive. 
            // If there is not enough food, the population will decrease. Extra food 
            // will increase the population.
            // Uses int because you can't have 2.5 people surviving.
            int survivors = food / 20;

            // Check to see if there are fewer people alive now than before.
            // If so, record that figure as the number of deaths.
            if (survivors < this.peasants)
            {
                this.deaths = this.peasants - survivors;
            }

            // Check to see if there are more people alive now than before.
            // If so, store the difference as number of births.
            if (survivors > this.peasants)
            {
                this.births = survivors - this.peasants;
            }

            // Update the number of peasants
            this.peasants = survivors;

            // Remove the grain that was eaten
            grain.GrainAvailable -= food;
        }
    }
}
