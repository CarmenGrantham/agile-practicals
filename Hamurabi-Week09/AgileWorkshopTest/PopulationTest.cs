﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileWorkshopTest;

namespace AgileWorkshopTest
{
    [TestFixture]
    class PopulationTest
    {

        [Test]
        public void PeasantsTest()
        {
            Population population = new Population();
            Assert.AreEqual(95, population.Peasants);

            population.Peasants = 10;
            Assert.AreEqual(10, population.Peasants);
        }

        [Test]
        public void DeathsTest()
        {
            Population population = new Population();
            Assert.AreEqual(0, population.Deaths);

            population.FeedPopulation(10, new Grain());
            Assert.AreNotEqual(0, population.Deaths);
        }


        [Test]
        public void BirthsTest()
        {
            Population population = new Population();
            Assert.AreEqual(0, population.Births);

            population.FeedPopulation(10000, new Grain());
            Assert.AreEqual(405, population.Births);
            Assert.AreEqual(500, population.Peasants);
        }


    }
}
