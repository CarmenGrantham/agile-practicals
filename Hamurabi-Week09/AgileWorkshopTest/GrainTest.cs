﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using AgileWorkshopTest;

namespace AgileWorkshopTest
{
    [TestFixture]
    class GrainTest
    {

        [Test]
        public void GrainAvailableTest()
        {
            Grain grain = new Grain();
            grain.GrainAvailable = 10;
            Assert.AreEqual(10, grain.GrainAvailable);

        }
    }
}
