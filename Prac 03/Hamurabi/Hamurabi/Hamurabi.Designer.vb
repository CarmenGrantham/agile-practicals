﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Hamurabi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SubmitButton = New System.Windows.Forms.Button()
        Me.YearLabel = New System.Windows.Forms.Label()
        Me.CurrentGrainTextLabel = New System.Windows.Forms.Label()
        Me.CurrentLandTextLabel = New System.Windows.Forms.Label()
        Me.CurrentPopulationTextLabel = New System.Windows.Forms.Label()
        Me.CurrentGrainLabel = New System.Windows.Forms.Label()
        Me.CurrentLandLabel = New System.Windows.Forms.Label()
        Me.CurrentPopulationLabel = New System.Windows.Forms.Label()
        Me.FoodTextLabel = New System.Windows.Forms.Label()
        Me.SeedTextLabel = New System.Windows.Forms.Label()
        Me.FoodBox = New System.Windows.Forms.TextBox()
        Me.SeedBox = New System.Windows.Forms.TextBox()
        Me.MainMenu = New System.Windows.Forms.MenuStrip()
        Me.FileMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoadGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MainMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'SubmitButton
        '
        Me.SubmitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SubmitButton.Location = New System.Drawing.Point(94, 220)
        Me.SubmitButton.Name = "SubmitButton"
        Me.SubmitButton.Size = New System.Drawing.Size(100, 30)
        Me.SubmitButton.TabIndex = 0
        Me.SubmitButton.Text = "Submit"
        Me.SubmitButton.UseVisualStyleBackColor = True
        '
        'YearLabel
        '
        Me.YearLabel.Location = New System.Drawing.Point(208, 34)
        Me.YearLabel.Name = "YearLabel"
        Me.YearLabel.Size = New System.Drawing.Size(64, 18)
        Me.YearLabel.TabIndex = 1
        Me.YearLabel.Text = "Year 0"
        '
        'CurrentGrainTextLabel
        '
        Me.CurrentGrainTextLabel.Location = New System.Drawing.Point(46, 52)
        Me.CurrentGrainTextLabel.Name = "CurrentGrainTextLabel"
        Me.CurrentGrainTextLabel.Size = New System.Drawing.Size(100, 15)
        Me.CurrentGrainTextLabel.TabIndex = 2
        Me.CurrentGrainTextLabel.Text = "Current Grain:"
        Me.CurrentGrainTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CurrentLandTextLabel
        '
        Me.CurrentLandTextLabel.Location = New System.Drawing.Point(46, 72)
        Me.CurrentLandTextLabel.Name = "CurrentLandTextLabel"
        Me.CurrentLandTextLabel.Size = New System.Drawing.Size(100, 15)
        Me.CurrentLandTextLabel.TabIndex = 3
        Me.CurrentLandTextLabel.Text = "Current Land:"
        Me.CurrentLandTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CurrentPopulationTextLabel
        '
        Me.CurrentPopulationTextLabel.Location = New System.Drawing.Point(46, 92)
        Me.CurrentPopulationTextLabel.Name = "CurrentPopulationTextLabel"
        Me.CurrentPopulationTextLabel.Size = New System.Drawing.Size(100, 15)
        Me.CurrentPopulationTextLabel.TabIndex = 4
        Me.CurrentPopulationTextLabel.Text = "Current Population:"
        Me.CurrentPopulationTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CurrentGrainLabel
        '
        Me.CurrentGrainLabel.Location = New System.Drawing.Point(152, 52)
        Me.CurrentGrainLabel.Name = "CurrentGrainLabel"
        Me.CurrentGrainLabel.Size = New System.Drawing.Size(100, 15)
        Me.CurrentGrainLabel.TabIndex = 5
        Me.CurrentGrainLabel.Text = "0"
        Me.CurrentGrainLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CurrentLandLabel
        '
        Me.CurrentLandLabel.Location = New System.Drawing.Point(152, 72)
        Me.CurrentLandLabel.Name = "CurrentLandLabel"
        Me.CurrentLandLabel.Size = New System.Drawing.Size(100, 15)
        Me.CurrentLandLabel.TabIndex = 6
        Me.CurrentLandLabel.Text = "0"
        Me.CurrentLandLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CurrentPopulationLabel
        '
        Me.CurrentPopulationLabel.Location = New System.Drawing.Point(152, 92)
        Me.CurrentPopulationLabel.Name = "CurrentPopulationLabel"
        Me.CurrentPopulationLabel.Size = New System.Drawing.Size(100, 15)
        Me.CurrentPopulationLabel.TabIndex = 7
        Me.CurrentPopulationLabel.Text = "0"
        Me.CurrentPopulationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FoodTextLabel
        '
        Me.FoodTextLabel.Location = New System.Drawing.Point(23, 140)
        Me.FoodTextLabel.Name = "FoodTextLabel"
        Me.FoodTextLabel.Size = New System.Drawing.Size(100, 15)
        Me.FoodTextLabel.TabIndex = 8
        Me.FoodTextLabel.Text = "Food:"
        Me.FoodTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'SeedTextLabel
        '
        Me.SeedTextLabel.Location = New System.Drawing.Point(23, 160)
        Me.SeedTextLabel.Name = "SeedTextLabel"
        Me.SeedTextLabel.Size = New System.Drawing.Size(100, 15)
        Me.SeedTextLabel.TabIndex = 9
        Me.SeedTextLabel.Text = "Seed:"
        Me.SeedTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FoodBox
        '
        Me.FoodBox.Location = New System.Drawing.Point(129, 138)
        Me.FoodBox.Name = "FoodBox"
        Me.FoodBox.Size = New System.Drawing.Size(70, 20)
        Me.FoodBox.TabIndex = 10
        '
        'SeedBox
        '
        Me.SeedBox.Location = New System.Drawing.Point(129, 158)
        Me.SeedBox.Name = "SeedBox"
        Me.SeedBox.Size = New System.Drawing.Size(70, 20)
        Me.SeedBox.TabIndex = 11
        '
        'MainMenu
        '
        Me.MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileMenuItem})
        Me.MainMenu.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu.Name = "MainMenu"
        Me.MainMenu.Size = New System.Drawing.Size(284, 24)
        Me.MainMenu.TabIndex = 14
        Me.MainMenu.Text = "File"
        '
        'FileMenuItem
        '
        Me.FileMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewGameToolStripMenuItem, Me.SaveGameToolStripMenuItem, Me.LoadGameToolStripMenuItem})
        Me.FileMenuItem.Name = "FileMenuItem"
        Me.FileMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileMenuItem.Text = "File"
        '
        'NewGameToolStripMenuItem
        '
        Me.NewGameToolStripMenuItem.Name = "NewGameToolStripMenuItem"
        Me.NewGameToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.NewGameToolStripMenuItem.Text = "New Game"
        '
        'SaveGameToolStripMenuItem
        '
        Me.SaveGameToolStripMenuItem.Name = "SaveGameToolStripMenuItem"
        Me.SaveGameToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SaveGameToolStripMenuItem.Text = "Save Game"
        '
        'LoadGameToolStripMenuItem
        '
        Me.LoadGameToolStripMenuItem.Name = "LoadGameToolStripMenuItem"
        Me.LoadGameToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.LoadGameToolStripMenuItem.Text = "Load Game"
        '
        'Hamurabi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.SeedBox)
        Me.Controls.Add(Me.FoodBox)
        Me.Controls.Add(Me.SeedTextLabel)
        Me.Controls.Add(Me.FoodTextLabel)
        Me.Controls.Add(Me.CurrentPopulationLabel)
        Me.Controls.Add(Me.CurrentLandLabel)
        Me.Controls.Add(Me.CurrentGrainLabel)
        Me.Controls.Add(Me.CurrentPopulationTextLabel)
        Me.Controls.Add(Me.CurrentLandTextLabel)
        Me.Controls.Add(Me.CurrentGrainTextLabel)
        Me.Controls.Add(Me.YearLabel)
        Me.Controls.Add(Me.SubmitButton)
        Me.Controls.Add(Me.MainMenu)
        Me.MainMenuStrip = Me.MainMenu
        Me.Name = "Hamurabi"
        Me.Text = "Hamurabi"
        Me.MainMenu.ResumeLayout(False)
        Me.MainMenu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SubmitButton As System.Windows.Forms.Button
    Friend WithEvents YearLabel As System.Windows.Forms.Label
    Friend WithEvents CurrentGrainTextLabel As System.Windows.Forms.Label
    Friend WithEvents CurrentLandTextLabel As System.Windows.Forms.Label
    Friend WithEvents CurrentPopulationTextLabel As System.Windows.Forms.Label
    Friend WithEvents CurrentGrainLabel As System.Windows.Forms.Label
    Friend WithEvents CurrentLandLabel As System.Windows.Forms.Label
    Friend WithEvents CurrentPopulationLabel As System.Windows.Forms.Label
    Friend WithEvents FoodTextLabel As System.Windows.Forms.Label
    Friend WithEvents SeedTextLabel As System.Windows.Forms.Label
    Friend WithEvents FoodBox As System.Windows.Forms.TextBox
    Friend WithEvents SeedBox As System.Windows.Forms.TextBox
    Friend WithEvents MainMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents FileMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
