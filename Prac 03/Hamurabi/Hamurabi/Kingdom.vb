﻿<Serializable()> Public Class Kingdom
    ' Set the constan variables. These shouldn't change between games.

    Const MIN_GRAIN As Integer = 20     ' Minimum amount of grain one person needs to survive
    Const MAX_LAND As Integer = 10      ' Maxium amount of land one person can manage
    Const SEED_REQUIRED As Integer = 2  ' How much seed is needed per acre of land

    ' Set intial values for the
    Private currentPopulation As Integer = 100
    Private currentGrain As Integer = 3000
    Private currentLand As Integer = 1000
    Private currentYear As Integer = 0

    ' Tracks the current population
    Public Property Population() As Integer
        Get
            Return currentPopulation
        End Get

        Set(ByVal Value As Integer)
            currentPopulation = Value
        End Set
    End Property

    ' Tracks the current land
    Public Property Land() As Integer
        Get
            Return currentLand
        End Get

        Set(ByVal Value As Integer)
            currentLand = Value
        End Set
    End Property

    ' Tracks the current grain
    Public Property Grain() As Integer
        Get
            Return currentGrain
        End Get

        Set(ByVal Value As Integer)
            currentGrain = Value
        End Set
    End Property

    ' Tracks the current year
    Public Property Year() As Integer
        Get
            Return currentYear
        End Get

        Set(ByVal Value As Integer)
            currentYear = Value
        End Set
    End Property

    ' useful to confirm if the amount of land to be farmed is viable, given the current population
    Public ReadOnly Property MaxSeed() As Integer
        Get
            Return SEED_REQUIRED * Math.Min(Land, Population * MAX_LAND)
        End Get
    End Property

    ' Takes a given amount of seed and food, and calculates the population and grain that is left
    ' as a result.
    Public Sub SimulateYear(ByVal seed As Integer, ByVal food As Integer)
        Dim harvest As Integer  ' How much seed was grown this year.

        ' Reduce the amount of grain by the food and seed that are to be used.
        Grain = Grain - food - seed
        ' If the user has assigned too much seed (more than the population can farm) use it all up,
        ' but only farm the maximum amount of land permitted by the amount of seed and the population
        If (seed > MaxSeed) Then
            seed = MaxSeed
        End If

        ' Calculate how much grain is produced.
        harvest = (seed / SEED_REQUIRED) * (2 + (5 * Rnd()))

        ' Feed the people, and work out how many have arrived or starved.
        Population = Population * food / (Population * MIN_GRAIN)

        'Increase the amount of grain available.s
        Grain = Grain + harvest

        ' Increase the year by 1.
        Year += 1
    End Sub
End Class
