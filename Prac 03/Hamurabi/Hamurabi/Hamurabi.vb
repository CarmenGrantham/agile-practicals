﻿Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization

Public Class Hamurabi
    Dim kingdom As Kingdom = New Kingdom()

    ' Default constructor
    Public Sub New()
        Me.InitializeComponent()

        ' Update the onscreen data to show initial values
        Me.YearLabel.Text = "Year: " & Me.kingdom.Year
        Me.CurrentGrainLabel.Text = Me.kingdom.Grain
        Me.CurrentPopulationLabel.Text = Me.kingdom.Population
        Me.CurrentLandLabel.Text = Me.kingdom.Land
    End Sub

    Private Sub UpdateGame()
        Dim food As Integer         ' The amount of grain to be used as food
        Dim seed As Integer         ' The amount of garin to be used to grow next season's crop

        ' As there is a chance that the user will have entered non-numbric (or at least non integer)
        ' values, this is in a Try/Catch block to pick up mistakes elegantly
        Try
            food = Convert.ToInt32(Me.FoodBox.Text) ' Read in the food and explicity convert to an Integer
            seed = Convert.ToInt32(Me.SeedBox.Text) ' Read in the seed and explicity convert to an Integer

            ' Check to make sure the user hasn't tried to trick us with negative values
            If (food >= 0 And seed >= 0) Then
                ' Check to make sure that they haven't entered an impossible amount
                If (food + seed <= Me.kingdom.Grain) Then
                    ' Time to update the situation
                    Me.kingdom.SimulateYear(seed, food)

                    ' Update the onscreen data
                    Me.DisplayData()

                    ' Check for an end game condition
                    If (Me.kingdom.Population <= 0) Then
                        MessageBox.Show("Everyone has died. Your reign has ended.")

                        ' Disable the new year button
                        Me.SubmitButton.Enabled = False
                    End If
                Else
                    MessageBox.Show("You only have " & Me.kingdom.Grain & " bushels of grain to use.")
                End If
            Else
                MessageBox.Show("The amount of food and seed needs to be 0 or more.")
            End If

        Catch ex As Exception
            MessageBox.Show("The amount of food and seed need to be whole numbers.")
        End Try

        ' Clear the input boxes for new data (this has to happen whatever the input)
        Me.FoodBox.Text = ""
        Me.SeedBox.Text = ""

    End Sub

    Public Sub DisplayData()
        ' Update the onscreen data
        Me.YearLabel.Text = "Year: " & Me.kingdom.Year
        Me.CurrentGrainLabel.Text = Me.kingdom.Grain
        Me.CurrentPopulationLabel.Text = Me.kingdom.Population
        Me.CurrentLandLabel.Text = Me.kingdom.Land
    End Sub

    Private Sub SubmitButton_Click(sender As System.Object, e As System.EventArgs) Handles SubmitButton.Click
        ' To keep this clean, call the main update method rather than add the code here
        Me.UpdateGame()
    End Sub

    Private Sub NewGame_Click(sender As System.Object, e As System.EventArgs) Handles NewGameToolStripMenuItem.Click
        ' Starts a new game.
        Me.kingdom = New Kingdom() ' Create a new instance of Kingdom, clearing the data.
        Me.DisplayData() ' Update what is on screen
        Me.SubmitButton.Enabled = True
    End Sub

    Private Sub SaveGame_Click(sender As System.Object, e As System.EventArgs) Handles SaveGameToolStripMenuItem.Click
        ' Save game routine.

        Dim filename As String = "savegame.sti" ' Default filename.

        ' Optional code
        ' This creates a dialog so as to ask the user where they want to save the file
        Dim dialog As SaveFileDialog = New SaveFileDialog()

        ' Show the dialog, and if they selected a save file ...
        If dialog.ShowDialog() = DialogResult.OK Then
            ' Grab the filename
            filename = dialog.FileName

            ' Open a file stream to write the data to, using the specified filename
            Dim stream As New FileStream(filename, FileMode.Create)

            ' Specify that we want to save this as a binary file
            Dim format As New BinaryFormatter

            Try
                ' Write the object to the file. In this case I want to sve the Kingdom object,
                ' as it contains the current game state.
                format.Serialize(stream, Me.kingdom)

                ' If it didn't work, catch some of tthe common errors ...
            Catch ex As ArgumentNullException
                MsgBox("The save game could not be accessed")
            Catch ex As SerializationException
                MsgBox("The game could not be saved")
            Finally

                ' And whatever happens, make sure to close the data stream
                stream.Close()
            End Try

        End If
    End Sub

    Private Sub LoadGame_Click(sender As System.Object, e As System.EventArgs) Handles LoadGameToolStripMenuItem.Click
        ' Load game routine

        Dim filename As String = "savegame.sti" ' Default filename.
        Dim stream As FileStream                ' Stream we'll be reading from

        ' Optional code
        ' This creates a dialog so as to ask the user while saved game to load
        Dim dialog As OpenFileDialog = New OpenFileDialog()

        ' Show the dialog, and if they selected a save file ...
        If dialog.ShowDialog() = DialogResult.OK Then
            ' Grab the filename
            filename = dialog.FileName

            ' Check to see if the file exists. It will, but I left this in anyway.
            If File.Exists(filename) Then

                ' Open a file stream to read the data from, using the specified filename
                stream = New FileStream(filename, FileMode.Open)
                Try
                    ' Specify the format
                    Dim format As New BinaryFormatter
                    ' Read in the saved version of kindom, and replace the game's version with the saved one
                    Me.kingdom = format.Deserialize(stream)
                    ' Now that the new game has been uploaded, display the file
                    Me.DisplayData()

                    ' Catch any errors
                Catch anex As ArgumentNullException
                    MsgBox("The save file could not be accessed")
                Catch ex As SerializationException
                    MsgBox("The application failed to load the save game")
                Finally
                    ' Make sure to close the data stream
                    stream.Close()
                End Try
            End If

        End If
    End Sub
End Class
