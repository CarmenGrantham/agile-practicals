Public Class HelloPerson2
    Inherits System.Windows.Forms.Form

    Dim myPerson As Person = New Person ' An instance of the Person Class. Used to store the person's name.

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        UpdatePerson()

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents pctWorld As System.Windows.Forms.PictureBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents btnSubmit As System.Windows.Forms.Button
    Friend WithEvents EventLog1 As System.Diagnostics.EventLog
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(HelloPerson2))
        Me.lblMessage = New System.Windows.Forms.Label
        Me.pctWorld = New System.Windows.Forms.PictureBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.btnSubmit = New System.Windows.Forms.Button
        Me.EventLog1 = New System.Diagnostics.EventLog
        CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblMessage
        '
        Me.lblMessage.Location = New System.Drawing.Point(184, 104)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(240, 64)
        Me.lblMessage.TabIndex = 0
        '
        'pctWorld
        '
        Me.pctWorld.BackgroundImage = CType(resources.GetObject("pctWorld.BackgroundImage"), System.Drawing.Image)
        Me.pctWorld.Location = New System.Drawing.Point(0, 0)
        Me.pctWorld.Name = "pctWorld"
        Me.pctWorld.Size = New System.Drawing.Size(200, 192)
        Me.pctWorld.TabIndex = 1
        Me.pctWorld.TabStop = False
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(176, 172)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(40, 16)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Name"
        '
        'txtName
        '
        Me.txtName.AcceptsReturn = True
        Me.txtName.Location = New System.Drawing.Point(216, 170)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(152, 20)
        Me.txtName.TabIndex = 3
        Me.txtName.Text = ""
        '
        'btnSubmit
        '
        Me.btnSubmit.Location = New System.Drawing.Point(376, 168)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.Size = New System.Drawing.Size(48, 24)
        Me.btnSubmit.TabIndex = 4
        Me.btnSubmit.Text = "Submit"
        '
        'EventLog1
        '
        Me.EventLog1.SynchronizingObject = Me
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(432, 237)
        Me.Controls.Add(Me.btnSubmit)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.lblMessage)
        Me.Controls.Add(Me.pctWorld)
        Me.Name = "Form1"
        Me.Text = "Hello Person"
        CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    ' -------------------------------------------------------------------------------
    ' UpdatePerson() checks myPerson in order to find out what the person's name is,
    ' then tailors the ouput to suit.

    Private Sub UpdatePerson()
        ' Check to see if no name has been provided, and comment appropriately.
        If (myPerson.getName() = "") Then
            ' No name has been stored, so just say hello.
            lblMessage.Text = "Hello, unknown person"
        ElseIf (myPerson.getName() = "Bill Paxton") Then ' Otherwise, check to see if the name was Bill Paxton ...
            ' ... and comment appropriately.
            lblMessage.Text = "Hello, Bill Paxton. You were great in Aliens."
        Else ' If there is a name, and it isn't Bill Paxton, then just give a generic hello.
            lblMessage.Text = "Hello, " & myPerson.getName()
        End If
    End Sub

    ' -------------------------------------------------------------------------------
    ' btnSubmit_Click() is run when the "Submit" button is clicked.

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        ' Store the value submitted in myPerson.
        myPerson.setName(txtName.Text)
        ' Say hello.
        UpdatePerson()
        ' Clear the text field.
        txtName.Text = ""
    End Sub
End Class
