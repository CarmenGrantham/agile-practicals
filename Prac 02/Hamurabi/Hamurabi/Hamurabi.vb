﻿Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization

Public Class Hamurabi



    Dim kingdom As Kingdom = New Kingdom()

    ' Default constructor
    Public Sub New()
        Me.InitializeComponent()

        Me.DisplayData()

    End Sub

    Private Sub UpdateGame()
        Dim food As Integer         ' The amount of grain to be used as food
        Dim seed As Integer         ' The amount of grain to be used to grow next season's crop

        ' As there is a chance that the user will have entered non-numeric (or at least non integer)
        ' values, this is in a Try/Catch block to pick up mistakes elegantly
        Try
            food = Convert.ToInt32(Me.FoodBox.Text) ' Read in the food and explicity convert to an Integer
            seed = Convert.ToInt32(Me.SeedBox.Text) ' Read in the seed and explicity convert to an Integer

            ' Check to make sure the user hasn't tried to trick us with negative values
            If (food >= 0 And seed >= 0) Then
                ' Check to make sure that they haven't entered an impossible amount
                If (food + seed <= Me.kingdom.Grain) Then
                    ' Time to update the situation
                    Me.kingdom.SimulateYear(seed, food)

                    ' Update the onscreen data
                    Me.DisplayData()

                    ' Once population drops to 0 the game is over
                    If (Me.kingdom.Population <= 0) Then
                        MessageBox.Show("The game is over. Everyone has died.")
                        Me.SubmitButton.Enabled = False
                    End If

                Else
                    MessageBox.Show("You only have " & Me.kingdom.Grain & " bushels of grain to use.")
                End If
            Else
                MessageBox.Show("The amount of food and seed needs to be 0 or more.")
            End If

        Catch ex As Exception
            MessageBox.Show("The amount of food and seed needs to be whole numbers.")
        End Try

        ' Clear the input boxes for new data (this has to happen whatever the input)
        Me.FoodBox.Text = ""
        Me.SeedBox.Text = ""

    End Sub

    Private Sub SubmitButton_Click(sender As System.Object, e As System.EventArgs) Handles SubmitButton.Click
        ' To keep this clean, call the main update method rather than add the code here
        Me.UpdateGame()
    End Sub

    Private Sub DisplayData()

        ' Update the onscreen data
        Me.YearLabel.Text = "Year: " & Me.kingdom.Year
        Me.CurrentGrainLabel.Text = Me.kingdom.Grain
        Me.CurrentPopulationLabel.Text = Me.kingdom.Population
        Me.CurrentLandLabel.Text = Me.kingdom.Land
    End Sub

    Private Sub SaveToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SaveToolStripMenuItem.Click
        Dim stream As Stream
        Dim saveFileDialog As New SaveFileDialog()
        Dim format As New BinaryFormatter

        saveFileDialog.Filter = "sti files (*.sti)|*.sti|All files (*.*)|*.*"
        saveFileDialog.FilterIndex = 1
        saveFileDialog.RestoreDirectory = True

        If saveFileDialog.ShowDialog() = DialogResult.OK Then
            stream = saveFileDialog.OpenFile()
            If (stream IsNot Nothing) Then
                ' Write kingdom to file
                format.Serialize(stream, Me.kingdom)

                ' close stream
                stream.Close()
            End If
        End If

    End Sub

    Private Sub LoadToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LoadToolStripMenuItem.Click
        Dim stream As FileStream
        Dim openFileDialog As New OpenFileDialog()
        Dim format As New BinaryFormatter

        openFileDialog.Filter = "sti files (*.sti)|*.sti|All files (*.*)|*.*"
        openFileDialog.FilterIndex = 1
        openFileDialog.RestoreDirectory = True

        If (openFileDialog.ShowDialog = System.Windows.Forms.DialogResult.OK) Then
            Try
                stream = openFileDialog.OpenFile()
                If (stream IsNot Nothing) Then
                    ' Insert code to read the stream here
                    Me.kingdom = format.Deserialize(stream)
                    Me.DisplayData()
                    Me.SubmitButton.Enabled = True
                End If
            Catch Ex As Exception
                MessageBox.Show("Cannot read file from disk. Original error: " & Ex.Message)
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open.
                If (stream IsNot Nothing) Then
                    stream.Close()
                End If
            End Try
        End If
    End Sub

    Private Sub NewToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NewToolStripMenuItem.Click
        ' Restart the game be recreating Kingdom
        Me.kingdom = New Kingdom()

        ' Update displayed data to use new values
        DisplayData()
        Me.SubmitButton.Enabled = True
    End Sub
End Class
