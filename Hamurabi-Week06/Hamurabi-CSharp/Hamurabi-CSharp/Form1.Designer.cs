﻿namespace Hamurabi_CSharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.submitButton = new System.Windows.Forms.Button();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.seedTextBox = new System.Windows.Forms.TextBox();
            this.foodTextBox = new System.Windows.Forms.TextBox();
            this.currentPopulationLabel = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.currentLandLabel = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.currentGrainLabel = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.yearLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(109, 217);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 23;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(89, 167);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(32, 13);
            this.Label5.TabIndex = 22;
            this.Label5.Text = "Seed";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(90, 140);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(31, 13);
            this.Label4.TabIndex = 21;
            this.Label4.Text = "Food";
            // 
            // seedTextBox
            // 
            this.seedTextBox.Location = new System.Drawing.Point(148, 167);
            this.seedTextBox.Name = "seedTextBox";
            this.seedTextBox.Size = new System.Drawing.Size(100, 20);
            this.seedTextBox.TabIndex = 20;
            // 
            // foodTextBox
            // 
            this.foodTextBox.Location = new System.Drawing.Point(148, 140);
            this.foodTextBox.Name = "foodTextBox";
            this.foodTextBox.Size = new System.Drawing.Size(100, 20);
            this.foodTextBox.TabIndex = 19;
            // 
            // currentPopulationLabel
            // 
            this.currentPopulationLabel.AutoSize = true;
            this.currentPopulationLabel.Location = new System.Drawing.Point(137, 101);
            this.currentPopulationLabel.Name = "currentPopulationLabel";
            this.currentPopulationLabel.Size = new System.Drawing.Size(123, 13);
            this.currentPopulationLabel.TabIndex = 18;
            this.currentPopulationLabel.Text = "Current Population Label";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(25, 101);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(97, 13);
            this.Label3.TabIndex = 17;
            this.Label3.Text = "Current Population:";
            // 
            // currentLandLabel
            // 
            this.currentLandLabel.AutoSize = true;
            this.currentLandLabel.Location = new System.Drawing.Point(137, 84);
            this.currentLandLabel.Name = "currentLandLabel";
            this.currentLandLabel.Size = new System.Drawing.Size(97, 13);
            this.currentLandLabel.TabIndex = 16;
            this.currentLandLabel.Text = "Current Land Label";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(50, 84);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(71, 13);
            this.Label2.TabIndex = 15;
            this.Label2.Text = "Current Land:";
            // 
            // currentGrainLabel
            // 
            this.currentGrainLabel.AutoSize = true;
            this.currentGrainLabel.Location = new System.Drawing.Point(136, 64);
            this.currentGrainLabel.Name = "currentGrainLabel";
            this.currentGrainLabel.Size = new System.Drawing.Size(98, 13);
            this.currentGrainLabel.TabIndex = 14;
            this.currentGrainLabel.Text = "Current Grain Label";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(43, 64);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(72, 13);
            this.Label1.TabIndex = 13;
            this.Label1.Text = "Current Grain:";
            // 
            // yearLabel
            // 
            this.yearLabel.AutoSize = true;
            this.yearLabel.Location = new System.Drawing.Point(176, 23);
            this.yearLabel.Name = "yearLabel";
            this.yearLabel.Size = new System.Drawing.Size(58, 13);
            this.yearLabel.TabIndex = 12;
            this.yearLabel.Text = "Year Label";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.seedTextBox);
            this.Controls.Add(this.foodTextBox);
            this.Controls.Add(this.currentPopulationLabel);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.currentLandLabel);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.currentGrainLabel);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.yearLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button submitButton;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox seedTextBox;
        internal System.Windows.Forms.TextBox foodTextBox;
        internal System.Windows.Forms.Label currentPopulationLabel;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label currentLandLabel;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label currentGrainLabel;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label yearLabel;
    }
}

