﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hamurabi_CSharp
{
    public partial class Form1 : Form
    {
        Kingdom kingdom = new Kingdom();

        int currentYear = 1;

        public Form1()
        {
            InitializeComponent();
            loadLabels();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(seedTextBox.Text) || String.IsNullOrEmpty(foodTextBox.Text))
            {
                MessageBox.Show("Must enter Seed and Food values");
                return;
            }

            int seed = Convert.ToInt32(seedTextBox.Text);
            int food = Convert.ToInt32(foodTextBox.Text);

            if (seed <= 0 || food <= 0)
            {
                MessageBox.Show("Must enter Seed and Food values that are more than 0");
                return;
            }

            if ((seed + food) > kingdom.Grain)
            {
                MessageBox.Show("Seed and food exceeds value of grain (" + kingdom.Grain + ")");
                return;
            }

            currentYear++;
            kingdom.simulateYear(seed, food);

            loadLabels();
        }

        private void loadLabels()
        {
            yearLabel.Text = "Year " + currentYear;
            currentGrainLabel.Text = ""+ kingdom.Grain;
            currentLandLabel.Text = "" + kingdom.Land;
            currentPopulationLabel.Text = "" + kingdom.Population;
        }
    }
}
