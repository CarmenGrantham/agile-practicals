﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamurabi_CSharp
{
    class Kingdom
    {
        private const int MIN_GRAIN = 20;
        private const int MAX_LAND = 10;
        private const int SEED_REQUIRED = 2;

        //private int currentPopulation = 100;
        //private int currentGrain = 3000;
        //private int currentLand = 1000;

        public int Population { get; set; } = 100;
        public int Grain { get; set; } = 3000;
        public int Land { get; set; } = 1000;

        public int maxSeed()
        {
            return SEED_REQUIRED * Math.Min(Land, Population * MAX_LAND);
        }

        public void simulateYear(int seed, int food)
        {
            Grain = Grain - food - seed;
            if (seed > maxSeed())
            {
                seed = maxSeed();
            }
            int harvest = (seed / SEED_REQUIRED) * (2 + (5 * (int)new Random().NextDouble()));
            Grain += harvest;
        }



    }
}
