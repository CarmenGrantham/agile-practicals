Public Class Form1
    Inherits System.Windows.Forms.Form

    Dim myPerson As Person = New Person ' An instance of the PersonClass Class. Used to provide the person's name.

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        UpdatePerson()

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents pctWorld As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.lblMessage = New System.Windows.Forms.Label
        Me.pctWorld = New System.Windows.Forms.PictureBox
        Me.SuspendLayout()
        '
        'lblMessage
        '
        Me.lblMessage.Location = New System.Drawing.Point(184, 104)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(232, 64)
        Me.lblMessage.TabIndex = 0
        '
        'pctWorld
        '
        Me.pctWorld.BackgroundImage = CType(resources.GetObject("pctWorld.BackgroundImage"), System.Drawing.Image)
        Me.pctWorld.Location = New System.Drawing.Point(0, 0)
        Me.pctWorld.Name = "pctWorld"
        Me.pctWorld.Size = New System.Drawing.Size(200, 192)
        Me.pctWorld.TabIndex = 1
        Me.pctWorld.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(432, 237)
        Me.Controls.Add(Me.lblMessage)
        Me.Controls.Add(Me.pctWorld)
        Me.Name = "Form1"
        Me.Text = "Hello Person"
        Me.ResumeLayout(False)

    End Sub

#End Region

    ' -------------------------------------------------------------------------------
    ' UpdatePerson() checks myPerson in order to find out what the person's name is,
    ' then tailors the ouput to suit.

    Public Sub UpdatePerson()
        ' Check to see if no name has been provided, and comment appropriately.
        If (myPerson.getName() = "") Then
            ' No name has been stored, so just say hello.
            lblMessage.Text = "Hello, unknown person"
        ElseIf (myPerson.getName() = "Bill Paxton") Then ' Otherwise, check to see if the name was Bill Paxton ...
            ' ... and comment appropriately.
            lblMessage.Text = "Hello, Bill Paxton. You were great in Aliens."
        Else ' If there is a name, and it isn't Bill Paxton, then just give a generic hello.
            lblMessage.Text = "Hello, " & myPerson.getName()
        End If
    End Sub

End Class
