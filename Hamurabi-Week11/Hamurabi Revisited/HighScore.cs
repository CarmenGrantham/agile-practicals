﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hamurabi_Revisited
{
    class HighScore
    {
        private string name = "";
        private int score = 0;

        public HighScore()
        {
            // Default - nothing happens
        }

        public HighScore(string name, int score)
        {
            this.Name = name;
            this.Score = score;
        }

        /// <summary>
        /// Read/write property representing the name of the person who got the score
        /// </summary>
        public string Name
        {
            set
            {
                this.name = value;
            }
            get
            {
                return this.name;
            }
        }

        /// <summary>
        /// Read/write property representing the score achieved
        /// </summary>
        public int Score
        {
            set
            {
                this.score = value;
            }
            get
            {
                return this.score;
            }
        }
    }
}