﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Hamurabi_Revisited
{
    public partial class HighScores : Form
    {
        private Label[] highScoreCountLabel = new Label[10];
        private Label[] highScoreNameLabel = new Label[10];
        private Label[] highScoreScoreLabel = new Label[10];

        private ArrayList highScores = new ArrayList();

        public HighScores()
        {
            this.InitializeComponent();
            this.AddHighScoreLabels();      // Doing this by code to make an easy-to-update array

            //populate array (keeps things simple if I'm not working with null values)
            for (int i = 0; i < 10; i++)
            {
                highScores.Add(new HighScore());
            }

            this.LoadHighScores();          // Reads in the high scores from a CSV file
            this.DisplayHighScores();       // Updates the high scores in the form
        }

        #region Check a new score and save if appropriate

        /// <summary>
        /// Checks to see if a score makes it to the top 10. Returns 
        /// the position if it does, -1 if it doesn't.
        /// </summary>
        public int CheckScore(int newScore)
        {
            int result = -1;                // Default value

            for (int i = 9; i >= 0; i--)    // Loop through the array
            {
                // Normal trick, check if the value is higher than what is in the list
                if (newScore > ((HighScore)highScores[i]).Score)
                {
                    result = i;
                }
            }

            return result;
        }

        /// <summary>
        /// Checks to see if a score makes it to the top 10. Adds it in
        /// if it does.
        /// </summary>
        public bool AddScore(string name, int score)
        {
            bool success = false;

            int position = this.CheckScore(score); // Check just to be sure.

            if (position > -1) // If it is worth adding ...
            {
                this.highScores.Insert(position, new HighScore(name, score)); // Add it to the array
                this.highScores.RemoveAt(10); // And remove the old one.
            }

            this.DisplayHighScores(); // Update the form

            this.SaveHighScores();  // Save the scores to the csv file.

            return success;
        }

        #endregion

        #region Labels to display scores

        private void AddHighScoreLabels()
        {
            for (int i = 0; i < 10; i++)
            {
                this.highScoreCountLabel[i] = new Label();
                this.highScoreCountLabel[i].Location = new System.Drawing.Point(10, 10 + (25 * i));
                this.highScoreCountLabel[i].Width = 25;
                this.Controls.Add(this.highScoreCountLabel[i]);

                this.highScoreNameLabel[i] = new Label();
                this.highScoreNameLabel[i].Location = new System.Drawing.Point(40, 10 + (25 * i));
                this.highScoreNameLabel[i].Width = 150;
                this.Controls.Add(this.highScoreNameLabel[i]);

                this.highScoreScoreLabel[i] = new Label();
                this.highScoreScoreLabel[i].Location = new System.Drawing.Point(200, 10 + (25 * i));
                this.highScoreScoreLabel[i].Width = 100;
                this.Controls.Add(this.highScoreScoreLabel[i]);
            }
        }
        #endregion

        #region Display the high scores

        // Adds the scores to the form.
        private void DisplayHighScores()
        {
            //Loop through an array

            for (int i = 0; i < 10; i++)
            {
                // Make sure a name was recorded.
                if (((HighScore)highScores[i]).Name != "")
                {
                    // display the details to the appropriate components.
                    highScoreCountLabel[i].Text = "" + (i + 1) + ".";
                    highScoreNameLabel[i].Text = ((HighScore)highScores[i]).Name;
                    highScoreScoreLabel[i].Text = "" + ((HighScore)highScores[i]).Score;
                }
            }
        }

        #endregion

        #region Load High Scores

        // Load the high scores from the CSV
        private void LoadHighScores()
        {
            try  // if the file doesn't exist, die quietly.
            {
                StreamReader input = File.OpenText("highscores.csv"); // Open for input.

                string line = input.ReadLine(); // Read the first line.
                int lineCount = 0; // Track how many have been read, and where we are up to.

                while (line != null) // Keep going until we run out of lines to read.
                {
                    string[] values = line.Split(','); // As this is a CSV, split based on the presence of the comma

                    ((HighScore)highScores[lineCount]).Name = values[0]; // First value is the name
                    ((HighScore)highScores[lineCount]).Score = Convert.ToInt32(values[1]); // Second value is the score

                    line = input.ReadLine(); // Read the next line
                    lineCount++; // Update the count.
                }

                input.Close(); // Close the file
            }
            catch(Exception e)
            {
                // This is ugly, but will do for now - just ignores any problems.
            }
        }

        #endregion

        #region Save High Scores

        // Save the high scores to the CSV. Works by just rewriting the whole thing.
        private void SaveHighScores()
        {
            try
            {
                StreamWriter output = File.CreateText("highscores.csv"); // Open the file

                for (int i = 0; i < highScores.Count; i++) // Loop through the array
                {
                    if (((HighScore)highScores[i]).Name != "") // Only record if there is a name
                    {
                        // Create the line.
                        string line = ((HighScore)highScores[i]).Name.Replace(',', ' ') + "," + ((HighScore)highScores[i]).Score;

                        // Save the line.
                        output.WriteLine(line);
                    }
                }

                output.Close(); // Close the file.
            }
            catch (Exception e)
            {
            }
        }

        #endregion

        // Quick override methode to hide rather than make the window close. Simplifies the code a bit.
        protected override void Dispose(bool disposing)
        {
            this.Hide();

            // (Not dispose, as I want the object to continue to exist)
        }

    }
}
