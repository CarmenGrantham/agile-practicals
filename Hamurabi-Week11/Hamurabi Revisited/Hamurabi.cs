﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Hamurabi_Revisited
{
    /// <summary>
    /// Hamurabi game, version 1.0.
    /// Developed for Agile Development with .NET at the University of South Australia
    /// </summary>
    public partial class Hamurabi : Form
    {
        private Kingdom kingdom = new Kingdom();    // The kingdom class tracks all of the relevant game variables.
        private HighScores myHighScores = new HighScores(); // Form element to display scores
        private HighScoreName myHighScoreName; // Form element to get player's name

        public Hamurabi()
        {
            myHighScoreName = new HighScoreName(myHighScores);
            InitializeComponent();
            DisplayState();
        }

        /// <summary>
        /// Displays the various values (such as births and deaths, current grain, etc)
        /// on the form. Called at the start of each year.
        /// </summary>
        private void DisplayState()
        {
            this.YearLabel.Text = "Year: " + this.kingdom.Year;
            this.Grain.Text = "" + this.kingdom.Grain.GrainAvailable;
            this.Population.Text = "" + this.kingdom.Population.Peasants;
            this.Acres.Text = "" + this.kingdom.Land.Acres;

            this.Births.Text = "" + this.kingdom.Population.Births;
            this.Deaths.Text = "" + this.kingdom.Population.Deaths;

            this.LandSellPrice.Text = "at " + this.kingdom.Land.Price + " bushells per acre";
            this.LandBuyPrice.Text = "at " + this.kingdom.Land.Price + " bushells per acre";

            this.Food.Text = "";
            this.AcresToBuy.Text = "";
            this.AcresToSell.Text = "";
            this.AcresToSow.Text = "";
        }

        /// <summary>
        /// Disbales the various form elements at the end of a game.
        /// </summary>
        private void DisableInput()
        {
            this.Food.Enabled = false;
            this.AcresToBuy.Enabled = false;
            this.AcresToSell.Enabled = false;
            this.AcresToSow.Enabled = false;

            this.YearButton.Text = "New Game";
        }

        /// <summary>
        /// Disbales the various form elements at the end of a game.
        /// </summary>
        private void EnableInput()
        {
            this.Food.Enabled = true;
            this.AcresToBuy.Enabled = true;
            this.AcresToSell.Enabled = true;
            this.AcresToSow.Enabled = true;

            this.YearButton.Text = "Advance Year";
        }

        /// <summary>
        /// Advances the year. As part of this, it calculates how much land
        /// was bought and sold, assigns grain to raming, assigns grain to
        /// food, and caluates both how much was grown and how many people
        /// lived and died.
        /// </summary>
        /// <param name="sender">The object that called the method</param>
        /// <param name="e">Information about the calling event</param>
        private void yearButton_Click(object sender, EventArgs e)
        {
            // Check to see if we're staring a new game
            if (this.YearButton.Text == "New Game")
            {
                myHighScoreName.Hide(); // Just in case it is visible.

                this.EnableInput();
                this.kingdom = new Kingdom();

                this.DisplayState();
            }
            else
            {
                // Move the year on one.
                this.kingdom.AdvanceYear();

                // Buy and sell land
                this.kingdom.Land.BuyAcres(Convert.ToInt32(this.AcresToBuy.Text), this.kingdom.Grain);
                this.kingdom.Land.SellAcres(Convert.ToInt32(this.AcresToSell.Text), this.kingdom.Grain);

                // Assign seed and land to grow food
                this.kingdom.Land.SowCrops(Convert.ToInt32(this.AcresToSow.Text), this.kingdom.Grain);
                this.kingdom.Land.ReapGrain(this.kingdom.Grain);

                // Feed the peasants and calculate how many lived and died.
                this.kingdom.Population.FeedPopulation(Convert.ToInt32(this.Food.Text), this.kingdom.Grain);

                string disasters = this.kingdom.Disasters;

                if (disasters != "")
                {
                    MessageBox.Show("My humble apologies.\n\n" + disasters);
                }

                this.kingdom.Land.SetLandPrice();

                // Update the form with the new grain, land and population details
                this.DisplayState();

                if (this.kingdom.GameOver)
                {
                    // Game over code
                    this.DisableInput();

                    // Check high scores. If it is one ...
                    if (this.myHighScores.CheckScore(this.kingdom.Score) > -1)
                    {
                        myHighScoreName.Score = this.kingdom.Score; // Tell the form the score.
                        myHighScoreName.Show(); // Show the player name input form.
                    }
                }
            }
        }

        private void highScorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myHighScores.Show();
        }
    }
}
