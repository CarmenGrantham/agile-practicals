﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hamurabi_Revisited
{
    /// <summary>
    /// The Land class tracks and manages the current land for the kingdom.
    /// As land is the key part of farming, it also manages the sowing and
    /// reaping of crops.
    /// </summary>
    class Land
    {
        private int acres = 1000;                   // Amount of land. 1000 is the default value.
        private int price = 20;                     // Price (in grain) of one acre of land. Defaults to 20.
        private int acresSown = 0;                  // Amount of land currently used to grow crops.
        private Random random = new Random();       // Random number generator

        /// <summary>
        /// Amount of land in acres
        /// </summary>
        public int Acres
        {
            set
            {
                this.acres = value;
            }
            get
            {
                return this.acres;
            }
        }

        public int Price
        {
            get
            {
                return this.price;
            }
        }

        public void SetLandPrice()
        {
            this.price = 17 + random.Next(11);
        }

        /// <summary>
        /// Read-only value to track how much and is currently devoted to farming.
        /// </summary>
        public int AcresSown
        {
            get
            {
                return this.acresSown;
            }
        }

        /// <summary>
        /// Sells some acres of land in return for grain
        /// </summary>
        /// <param name="acres">The amount of land to sell</param>
        /// <param name="grain">The Grain object to hold the new grain</param>
        public void SellAcres(int acres, Grain grain)
        {
            this.acres -= acres;
            grain.GrainAvailable += acres * this.price;
        }

        /// <summary>
        /// Handles the purchase of acres.
        /// </summary>
        /// <param name="acres">The number of acres to purchase</param>
        /// <param name="grain">The Grain object, from which the grain is removed</param>
        public void BuyAcres(int acres, Grain grain)
        {
            this.acres += acres;
            grain.GrainAvailable -= acres * this.price;
        }

        /// <summary>
        /// Puts land aside to grow crops and stores the value, removeing
        /// an appropriate amount of graun to cover the seed.
        /// </summary>
        /// <param name="acres">The number of acres to sow with seed</param>
        /// <param name="grain">The Grain object to get the seed from.</param>
        public void SowCrops(int acres, Grain grain)
        {
            this.acresSown = acres;
            grain.GrainAvailable -= acres * 2;
        }

        /// <summary>
        /// Get the grain after the land has been sown.
        /// </summary>
        /// <param name="grain">The Grain object to store the grain that was reaped</param>
        public void ReapGrain(Grain grain)
        {
            grain.GrainAvailable += this.acresSown * (random.Next(6) + 3);
            this.acresSown = 0;
        }

    }
}
