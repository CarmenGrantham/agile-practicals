﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hamurabi_Revisited
{
    /// <summary>
    /// The Kingdom class serves to track the different objects - land, grain and 
    /// population - needed to run the simulation.
    /// </summary>
    class Kingdom
    {
        private Grain grain = new Grain();                  // Tracks the grain available (in bushels)
        private Land land = new Land();                     // Tracks the current amount of land (in acres)
        private Population population = new Population();   // Tracks the current population
        private int year = 0;                               // The current year
        private Random random = new Random();               // Random number generator
        private bool stillAlive = true;                     // If you are still alive.


        /// <summary>
        /// Read-only instance of the Grain object.
        /// </summary>
        public Grain Grain
        {
            get
            {
                return this.grain;
            }
        }

        /// <summary>
        /// Read-only instance of the Land object
        /// </summary>
        public Land Land
        {
            get
            {
                return this.land;
            }
        }

        /// <summary>
        /// Read-only instance of the Population object
        /// </summary>
        public Population Population
        {
            get
            {
                return this.population;
            }
        }

        /// <summary>
        /// Read-only property to access the current year
        /// </summary>
        public int Year
        {
            get
            {
                return this.year;
            }
        }

        /// <summary>
        /// Read-only property to access whether the game is over or not.
        /// </summary>
        public bool GameOver
        {
            get
            {
                return !this.stillAlive;
            }
        }

        /// <summary>
        /// Increase the year by one.
        /// </summary>
        public void AdvanceYear()
        {
            this.year++;
        }

        /// <summary>
        /// Read-only property to access the game score.
        /// </summary>
        public int Score
        {
            get
            {
                return (((this.Population.Peasants * 10) + this.Grain.GrainAvailable + (this.Land.Acres * 4)) * this.year) / 2;
            }
        }

        /// <summary>
        /// Read-only property to calculate and access the description of any disasters that have befallen the player
        /// </summary>
        public string Disasters
        {
            get
            {
                string disasters = "";                      // Any disasters that may have occured.

                // Calculate deaths as percentage of old population. If more than 40% we have a rebellion.
                if (this.population.Deaths > 0)
                {
                    if ((this.population.Deaths + this.population.Peasants) * .4 <= this.population.Deaths)
                    {
                        disasters += "Your peasants are revolting! You have been removed from office and executed due to your mismanagement.\n";
                        this.stillAlive = false;
                    }
                }

                // These are only relevant if the population didn't kill you.

                if (this.stillAlive)
                {
                    // Calculate effect of rats.
                    int rats = random.Next(10);

                    // Reduce the amount of grain by the percentage eaten by the rats.
                    int oldGrain = this.grain.GrainAvailable;
                    this.grain.GrainAvailable = (int)(oldGrain * ((100 - rats) / 100.0));

                    disasters += "Rats have eaten " + (oldGrain - this.grain.GrainAvailable) + " bushels of grain!\n\n";


                    // Calculate if a plague has hit.
                    if (random.Next(100) <= 15)
                    {
                        // Plague has hit!
                        this.population.Peasants = this.population.Peasants / 2;
                        disasters += "A plague has hit your kingdom! Half of your population has died.\n\n";
                    }

                    // Check for game over due to lack of peasants
                    if (this.population.Peasants <= 0)
                    {
                        this.stillAlive = false;
                        disasters += "With no peasants left alive, your kingdom is no more.\n\n";
                    }

                    // Check for game over due to lack of food
                    if (this.grain.GrainAvailable <= 0)
                    {
                        this.stillAlive = false;
                        disasters += "With no grain left to feed your peasants, your kingdom is no more.\n\n";
                    }
                }

                return disasters;
            }
        }
    }
}
