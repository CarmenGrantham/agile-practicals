﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Hamurabi_Revisited
{
    public partial class HighScoreName : Form
    {
        private HighScores HighScoresForm = new HighScores(); // Used to know which form to inform when a name is entered.
        private int score = 0;

        public HighScoreName(HighScores highScores)
        {
            InitializeComponent();
            this.HighScoresForm = highScores;
        }

        /// <summary>
        /// Read and write property to access the game score.
        /// </summary>
        public int Score
        {
            set
            {
                score = value;
            }
            get
            {
                return score;
            }
        }

        // Save button actions.
        private void saveButton_Click(object sender, EventArgs e)
        {
            // Check to see if a name was entered.
            if (this.NameBox.Text != "")
            {
                // Inform the high score form of the name and score.
                this.HighScoresForm.AddScore(this.NameBox.Text, score);
            }

            // Hide this element.
            this.Hide();
        }
    }
}
