﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.outputLabel = New System.Windows.Forms.Label()
        Me.doSomethingButton = New System.Windows.Forms.Button()
        Me.inputBox = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'outputLabel
        '
        Me.outputLabel.AutoSize = True
        Me.outputLabel.Location = New System.Drawing.Point(22, 45)
        Me.outputLabel.MinimumSize = New System.Drawing.Size(200, 20)
        Me.outputLabel.Name = "outputLabel"
        Me.outputLabel.Size = New System.Drawing.Size(200, 20)
        Me.outputLabel.TabIndex = 0
        '
        'doSomethingButton
        '
        Me.doSomethingButton.Location = New System.Drawing.Point(44, 141)
        Me.doSomethingButton.Name = "doSomethingButton"
        Me.doSomethingButton.Size = New System.Drawing.Size(143, 23)
        Me.doSomethingButton.TabIndex = 1
        Me.doSomethingButton.Text = "Do Something!"
        Me.doSomethingButton.UseVisualStyleBackColor = True
        '
        'inputBox
        '
        Me.inputBox.Location = New System.Drawing.Point(25, 68)
        Me.inputBox.Name = "inputBox"
        Me.inputBox.Size = New System.Drawing.Size(229, 20)
        Me.inputBox.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.inputBox)
        Me.Controls.Add(Me.doSomethingButton)
        Me.Controls.Add(Me.outputLabel)
        Me.Name = "Form1"
        Me.Text = "Hello World Application"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents outputLabel As Label
    Friend WithEvents doSomethingButton As Button
    Friend WithEvents inputBox As TextBox
End Class
