using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;

namespace BarGraph
{
    public partial class Graph : Form
    {
        private int[] values = { 67, 80, 35, 100, 10 }; // Some values to work with. Presumably, these could come from somewhere else, but are hardcoded for testing.

        public Graph()
        {
            InitializeComponent();
        }
        
        // New OnPaint method
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            // The margins are to give some space rather than drawing right on the edge of the component.
            int leftMargin = 20;    
            int rightMargin = 20;
            int topMargin = 20;
            int bottomMargin = 20;

            // This are the maximum practical height and width, after we allow for some margins to make it neat.
            // Double is used here as 1 * 100 is very different from 1.1 * 100, even if we convert to an integer at the end of the process.
            double maxWidth = this.Width - leftMargin - rightMargin;
            double maxHeight = this.Height - topMargin - bottomMargin - 30;

            g.Clear(Color.WhiteSmoke);

            // Created this as a method so we can reuse it in the print functions.
            this.generateGraph(g, leftMargin, topMargin, maxHeight, maxWidth);

            // Just to repaint any additional features.
            base.OnPaint(e);
        }

        // Method that does the real work of drawing the graph.
        private void generateGraph(Graphics g, int leftMargin, int topMargin, double maxHeight, double maxWidth)
        {
            int range = 100; // This could normally be gained by looking at the range of values and picking the largest.

            // Maximum width of each individual column - we need room to fit them all. 
            double columnWidth = (maxWidth / values.Length);

            // Loop through the values we need to display
            for (int columnNumber = 0; columnNumber < values.Length; columnNumber++)
            {
                // Here we could set different colours for different columns, which is
                // why I'm setting them in the loop.
                Pen currentPen = new Pen(Color.Brown);
                Brush currentBrush = new SolidBrush(Color.BurlyWood);

                // Calculate teh height of this particular column.
                int columnHeight = (int)((maxHeight / range) * values[columnNumber]);

                // Work out the top left corner of the column.
                int y = (int)(topMargin + maxHeight - columnHeight);
                int x = (int)(leftMargin + (columnWidth * columnNumber));

                // Draw a filled rectangle for the column
                g.FillRectangle(currentBrush, x, y, (int)columnWidth, columnHeight);
                // Draw a border around it to make it look pretty
                g.DrawRectangle(currentPen, x, y, (int)columnWidth, columnHeight);
            }
        }

        // Makes resizing work by forcing a full redraw each time.
        private void Form1_Resize(object sender, EventArgs e)
        {
            this.Invalidate();
        }

        // Called when the print button is used.
        private void printButton_Click(object sender, EventArgs e)
        {
            // Create a new page
            PrintDocument pd = new PrintDocument();
            // Set the event to handle it.
            pd.PrintPage += new PrintPageEventHandler(this.PrintPageEvent);
            // Print the page.
            pd.Print();
        }

        // This is where the work of printing each individual page occurs.
        private void PrintPageEvent(object sender, PrintPageEventArgs ev)
        {
            Graphics g = ev.Graphics;

            // Work out the margins.
            int marginLeft = ev.PageSettings.Margins.Left;
            int marginRight = ev.PageSettings.Margins.Right;
            int marginTop = ev.PageSettings.Margins.Top;
            int marginBottom = ev.PageSettings.Margins.Bottom;

            // Work out the usable height and width
            double maxHeight = ev.PageSettings.PrintableArea.Height - marginTop - marginBottom;
            double maxWidth = ev.PageSettings.PrintableArea.Width - marginLeft - marginRight;

            // Draw the graph
            this.generateGraph(g, marginLeft, marginTop, maxHeight, maxWidth);

        }



    }
}