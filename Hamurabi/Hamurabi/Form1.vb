﻿Public Class Form1

    Dim kingdom As Kingdom = New Kingdom()
    Dim currentYear As Integer = 1

    Private Sub submitButton_Click(sender As Object, e As EventArgs) Handles submitButton.Click
        Dim seed As Integer
        Dim food As Integer

        If (String.IsNullOrEmpty(seedTextBox.Text) Or String.IsNullOrEmpty(foodTextBox.Text)) Then
            MsgBox("Must enter Seed and Food values")
            Return
        End If

        seed = Convert.ToInt32(seedTextBox.Text)
        food = Convert.ToInt32(foodTextBox.Text)

        If (seed <= 0 Or food <= 0) Then
            MsgBox("Must enter Seed and Food values that are greater than 0")
            Return
        End If

        If ((seed + food) > kingdom.Grain) Then
            MsgBox("Seed and food exceeds value of grain (" & kingdom.Grain & ")")
            Return
        End If

        currentYear = currentYear + 1

        kingdom.simulateYear(seed, food)

        loadLabels()
    End Sub

    Sub loadLabels()
        yearLabel.Text = "Year " & currentYear
        currentGrainLabel.Text = kingdom.Grain
        currentLandLabel.Text = kingdom.Land
        currentPopulationLabel.Text = kingdom.Population
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadLabels()
    End Sub
End Class
