﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.yearLabel = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.currentGrainLabel = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.currentLandLabel = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.currentPopulationLabel = New System.Windows.Forms.Label()
        Me.foodTextBox = New System.Windows.Forms.TextBox()
        Me.seedTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.submitButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'yearLabel
        '
        Me.yearLabel.AutoSize = True
        Me.yearLabel.Location = New System.Drawing.Point(188, 13)
        Me.yearLabel.Name = "yearLabel"
        Me.yearLabel.Size = New System.Drawing.Size(58, 13)
        Me.yearLabel.TabIndex = 0
        Me.yearLabel.Text = "Year Label"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(55, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Current Grain:"
        '
        'currentGrainLabel
        '
        Me.currentGrainLabel.AutoSize = True
        Me.currentGrainLabel.Location = New System.Drawing.Point(148, 54)
        Me.currentGrainLabel.Name = "currentGrainLabel"
        Me.currentGrainLabel.Size = New System.Drawing.Size(98, 13)
        Me.currentGrainLabel.TabIndex = 2
        Me.currentGrainLabel.Text = "Current Grain Label"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(62, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Current Land:"
        '
        'currentLandLabel
        '
        Me.currentLandLabel.AutoSize = True
        Me.currentLandLabel.Location = New System.Drawing.Point(149, 74)
        Me.currentLandLabel.Name = "currentLandLabel"
        Me.currentLandLabel.Size = New System.Drawing.Size(97, 13)
        Me.currentLandLabel.TabIndex = 4
        Me.currentLandLabel.Text = "Current Land Label"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(37, 91)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(97, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Current Population:"
        '
        'currentPopulationLabel
        '
        Me.currentPopulationLabel.AutoSize = True
        Me.currentPopulationLabel.Location = New System.Drawing.Point(149, 91)
        Me.currentPopulationLabel.Name = "currentPopulationLabel"
        Me.currentPopulationLabel.Size = New System.Drawing.Size(123, 13)
        Me.currentPopulationLabel.TabIndex = 6
        Me.currentPopulationLabel.Text = "Current Population Label"
        '
        'foodTextBox
        '
        Me.foodTextBox.Location = New System.Drawing.Point(160, 130)
        Me.foodTextBox.Name = "foodTextBox"
        Me.foodTextBox.Size = New System.Drawing.Size(100, 20)
        Me.foodTextBox.TabIndex = 7
        '
        'seedTextBox
        '
        Me.seedTextBox.Location = New System.Drawing.Point(160, 157)
        Me.seedTextBox.Name = "seedTextBox"
        Me.seedTextBox.Size = New System.Drawing.Size(100, 20)
        Me.seedTextBox.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(102, 130)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Food"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(101, 157)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Seed"
        '
        'submitButton
        '
        Me.submitButton.Location = New System.Drawing.Point(121, 207)
        Me.submitButton.Name = "submitButton"
        Me.submitButton.Size = New System.Drawing.Size(75, 23)
        Me.submitButton.TabIndex = 11
        Me.submitButton.Text = "Submit"
        Me.submitButton.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.submitButton)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.seedTextBox)
        Me.Controls.Add(Me.foodTextBox)
        Me.Controls.Add(Me.currentPopulationLabel)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.currentLandLabel)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.currentGrainLabel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.yearLabel)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents yearLabel As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents currentGrainLabel As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents currentLandLabel As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents currentPopulationLabel As Label
    Friend WithEvents foodTextBox As TextBox
    Friend WithEvents seedTextBox As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents submitButton As Button
End Class
