﻿Public Class Kingdom
    Private Const MIN_GRAIN As Integer = 20
    Private Const MAX_LAND As Integer = 10
    Private Const SEED_REQUIRED As Integer = 2

    Private currentPopulation As Integer = 100
    Private currentGrain As Integer = 3000
    Private currentLand As Integer = 1000

    Public Property Population As Integer
        Get
            Return currentPopulation
        End Get
        Set(value As Integer)
            currentPopulation = value
        End Set
    End Property

    Public Property Grain As Integer
        Get
            Return currentGrain
        End Get
        Set(value As Integer)
            currentGrain = value
        End Set
    End Property
    Public Property Land As Integer
        Get
            Return currentLand
        End Get
        Set(value As Integer)
            currentLand = value
        End Set
    End Property

    Public ReadOnly Property MaxSeed() As Integer
        Get
            ' Math.Min returns smallest of the 2 values
            Return SEED_REQUIRED * Math.Min(Land, Population * MAX_LAND)
        End Get
    End Property

    Sub simulateYear(ByRef seed As Integer, ByRef food As Integer)
        Dim harvest As Integer
        Grain = Grain - food - seed
        If (seed > MaxSeed) Then
            seed = MaxSeed
        End If

        harvest = (seed / SEED_REQUIRED) * (2 + (5 * Rnd()))
        Population = Population * food / (Population * MIN_GRAIN)
        Grain = Grain + harvest

    End Sub

End Class
